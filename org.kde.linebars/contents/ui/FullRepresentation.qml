/*
 *   Copyright 2021 David Redondo <kde@david-redondo.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2

import org.kde.kirigami 2.15 as Kirigami
import org.kde.quickcharts 1.0 as Charts
import org.kde.ksysguard.faces 1.0 as Faces
import org.kde.ksysguard.formatter 1.0 as Formatter
import org.kde.ksysguard.sensors 1.0 as Sensors

Faces.SensorFace {
    id: root
    contentItem: ColumnLayout {
        Kirigami.Heading {
            Layout.fillWidth: true
            text: root.controller.title
            level: 2
            visible: root.controller.showTitle
            horizontalAlignment: Text.AlignHCenter
        }

        Sensors.SensorDataModel {
            id: sensorDataModel
            sensors: root.controller.highPrioritySensorIds
            sensorColors: root.controller.sensorColors
        }


        LineBars {
            id: chart
            Layout.fillWidth: true
            Layout.fillHeight: true
            colorSource: root.colorSource
            sensorDataModel: sensorDataModel
        }

        Faces.ExtendedLegend {
            Layout.fillWidth: true
            visible: root.controller.faceConfiguration.showLegend
            sourceModel: sensorDataModel
            colorSource: root.colorSource
            sensorIds: root.controller.lowPrioritySensorIds
        }
    }
}
