/*
 *   Copyright 2021 David Redondo <kde@david-redondo.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


import QtQuick 2.15
import QtQml 2.15

import org.kde.ksysguard.sensors 1.0 as Sensors
import org.kde.quickcharts 1.0 as Charts

Charts.BarChart {
    id: chart
    property var sensorDataModel
    direction: Charts.XYChart.ZeroAtEnd
    barWidth: 4
    stacked: true

     yRange {
        from: root.controller.faceConfiguration.rangeFrom
        to: root.controller.faceConfiguration.rangeTo
        automatic: root.controller.faceConfiguration.rangeAuto
    }

    Instantiator {
        model: sensorDataModel.sensors
        delegate:  Charts.HistoryProxySource {
            source: Charts.ModelSource {
                model: sensorDataModel
                column: index
                role: Sensors.SensorDataModel.Value
            }
            interval: sensorDataModel.ready ? sensorDataModel.headerData(0, Qt.Horizontal, Sensors.SensorDataModel.UpdateInterval) : 0
            maximumHistory: chart.width / (chart.barWidth + chart.barWidth / 2)
            fillMode: Charts.HistoryProxySource.FillFromStart
        }
        onObjectAdded: {
            chart.insertValueSource(index, object)
        }
        onObjectRemoved: {
            chart.removeValueSource(object)
        }
    }
}
