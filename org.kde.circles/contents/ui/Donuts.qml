/*
 *   Copyright 2020 David Redondo <kde@david-redondo.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


import QtQuick 2.15
import QtQml 2.15
import QtQuick.Layouts 1.15

import org.kde.ksysguard.sensors 1.0 as Sensors
import org.kde.quickcharts 1.0 as Charts
import org.kde.quickcharts.controls 1.0 as ChartsControls

ChartsControls.PieChartControl {
    id: chart

    chart.colorSource: root.colorSource
    chart.indexingMode: Charts.Chart.IndexEachSource
    chart.backgroundColor: Qt.rgba(0.0, 0.0, 0.0, 0.2)
    chart.smoothEnds: true
    chart.fromAngle: root.controller.faceConfiguration.fromAngle
    chart.toAngle: root.controller.faceConfiguration.toAngle
    text: totalSensor.formattedValue

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    range {
        from: root.controller.faceConfiguration.rangeFrom
        to: root.controller.faceConfiguration.rangeTo
        automatic: root.controller.faceConfiguration.rangeAuto
    }

    Sensors.Sensor {
        id: totalSensor
        sensorId: root.controller.totalSensors[0] || ""
    }

    Instantiator {
        model: sensorDataModel.sensors
        delegate: Charts.ModelSource {
            model: sensorDataModel
            column: index
            role: Sensors.SensorDataModel.Value
        }
        onObjectAdded: {
            console.log(root)
            console.log(root.controller)
            console.log(root.controller.highPrioritySensorIds)
            chart.chart.insertValueSource(index, object)
        }
        onObjectRemoved: {
            chart.chart.removeValueSource(object)
        }
    }

}
